"use strict";

const   fs            = require("fs"),
        _             = require("lodash"),
        core          = require("./packages/ligament-core"),
        Checker       = core.Checker,
        ErrorReporter = core.ErrorReporter;

let code = fs.readFileSync("test/assets/test1.js", "utf8");
let checker = Checker(code, ["compatible-types-filter", "or-type-filter", "constant-filter", "core-type-filter"]);
checker.check();

let errorReporter = new ErrorReporter(code);
errorReporter.printErrors(checker.errors);

if(checker.errors.length === 0) {
    console.log(_.mapValues(checker.types, (type) => {
        return type.getPrettyName();
    }));
}
