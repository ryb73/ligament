"use strict";

module.exports = CoreTypeFilter;

const   coreTypes = require("ligament-core-types"),
        number    = coreTypes.number,
        string    = coreTypes.string,
        boolean   = coreTypes.boolean,
        any       = coreTypes.any;

function CoreTypeFilter() {
    return { checkAdd, checkSubtract, checkStrictEquality };

    function checkAdd(registerError, leftType, rightType) {
        if(leftType.any && rightType.any)
            return any.getAnyType();

        if(isNumberType(leftType) && isNumberType(rightType))
            return number.getNumberType();

        if(isStringType(leftType) && isStringType(rightType))
            return string.getStringType();
    }

    function checkSubtract(registerError, leftType, rightType) {
        if(isNumberType(leftType) && isNumberType(rightType))
            return number.getNumberType();
    }

    function isNumberType(type) {
        return type.getCoreType() === "number" || type.any;
    }

    function isStringType(type) {
        return type.getCoreType() === "string" || type.any;
    }

    function checkStrictEquality(registerError, leftType, rightType) {
        if(leftType.any || rightType.any ||
            (isNumberType(leftType) && isNumberType(rightType)) ||
            (isStringType(leftType) && isStringType(rightType)))
        {
            return boolean.getBooleanType();
        }
    }
}