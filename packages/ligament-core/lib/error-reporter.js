"use strict";

const   _             = require("lodash"),
        LetterCounter = require("./letter-counter"),
        Type          = require("./type");

function ErrorReporter($code) {
    let code = $code;
    $code = null;

    return Object.freeze({ printErrors });

    function printErrors(errors) {
        if(errors.length === 0) {
            console.log("No errors found! \u{1f44c}");
        }

        errors.forEach(printError);
    }

    function printError(error) {
        let lineInfo = getLineInfo(error.node.start);

        console.log(`on ${lineInfo.lineNumber}:${lineInfo.errorPosInLine}:`);

        console.log(lineInfo.lineContents);
        let arrow = _.repeat(" ", lineInfo.errorPosInLine - 1);
        let erroneousSegmentLength = error.node.end - error.node.start;
        arrow += _.repeat("^", Math.min(erroneousSegmentLength, lineInfo.lineContents.length));
        console.log(arrow);

        let message = error.message;
        let printableTypes = getPrintableTypes(error.types);
        let longTypes = [];
        let counter = new LetterCounter();
        printableTypes.forEach((printableType) => {
            let typeIdentifier;
            if(isMultiline(printableType)) {
                typeIdentifier = counter.next();
                longTypes.push({ printableType, typeIdentifier });
            } else {
                typeIdentifier = "`" + printableType + "`";
            }

            message = message.replace("%t", typeIdentifier);
        });

        if(longTypes.length > 0) {
            message += ", where";
        }

        console.log(message);
        longTypes.forEach((longType) => {
            console.log(`${longType.typeIdentifier} = ${longType.printableType}`);
            console.log();
        });
        console.log();
    }

    function getLineInfo(errorPos) {
        // Find line number and line beginning position
        let lineStartPos = 0,
            lineBreaks = 0,
            inMiddleOfLineBreak = false;
        for(let i = errorPos; i >= 0; --i) {
            if(!isLineBreakChar(code[i])) {
                if(inMiddleOfLineBreak) {
                    ++lineBreaks;
                    inMiddleOfLineBreak = false;
                }

                continue;
            }

            inMiddleOfLineBreak = true;
            if(lineStartPos === 0)
                lineStartPos = i + 1;
        }

        if(inMiddleOfLineBreak)
            ++lineBreaks;

        // Find line ending position
        let lineEndPos = code.length;
        for(let i = errorPos; i < code.length; ++i) {
            if(isLineBreakChar(code[i])) {
                lineEndPos = i;
                break;
            }
        }

        return {
            errorPosInLine: errorPos - lineStartPos + 1,
            lineNumber: lineBreaks + 1,
            lineContents: code.substring(lineStartPos, lineEndPos),
        };
    }

    function isLineBreakChar(char) {
        return char === "\n" || char === "\r";
    }

    function isMultiline(string) {
        return _.some(string, isLineBreakChar);
    }

    function getPrintableTypes(types) {
        return _.invokeMap(types, "getPrettyName");
    }
}

module.exports = ErrorReporter;