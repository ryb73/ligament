"use strict";

const   _        = require("lodash"),
        typedefs = require("./typedefs");

function Type($definition) {
    let definition = $definition;
    $definition = null;

    return Object.freeze({ unify });

    function unify(otherType) {

    }
}

Type.isAddable = (type) => {
    return type.capabilities.numAdd || type.capabilities.stringAdd;
};

Type.prettyName = (type) => {
    for (let key in typedefs) {
        if(_.isEqual(type, typedefs[key]))
            return key;
    }

    return JSON.stringify(type, null, 2);
};

module.exports = Type;