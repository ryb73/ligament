"use strict";

module.exports = FilterAggregator;

const   _              = require("lodash"),
        typeError      = require("ligament-core-types").typeError,
        FallbackFilter = require("./fallback-filter");

function FilterAggregator(filterNames) {
    let filters = _(filterNames)
        .map((filterName) => "ligament-" + filterName)
        .map(require)
        .map((constructor) => new constructor(this))
        .value();

    filters.push(new FallbackFilter());

    this.checkTruthiness     = genericLoop("checkTruthiness");
    this.checkAdd            = genericLoop("checkAdd");
    this.checkSubtract       = genericLoop("checkSubtract");
    this.checkStrictEquality = genericLoop("checkStrictEquality");

    function genericLoop(funcName) {
        return (registerError, ...args) => {
            for(let filter of filters) {
                if(!filter[funcName])
                    continue;

                let result = filter[funcName].apply(filter, [ registerError, ...args ]);
                if(result)
                    return result;
            }

            registerError(`Internal error: no resolution for ${funcName}`);
            return typeError.getTypeErrorType();
        };
    }
}