"use strict";

module.exports = FallbackFilter;

const   coreTypes = require("ligament-core-types"),
        boolean   = coreTypes.boolean,
        typeError = coreTypes.typeError;

function FallbackFilter() {
    return { checkTruthiness, checkAdd, checkSubtract };

    function checkTruthiness(registerError, type) {
        return boolean.getBooleanType();
    }

    function checkAdd(registerError, leftType, rightType) {
        registerError("The + operator is incompatible with types %t and %t", [ leftType, rightType ]);
        return typeError.getTypeErrorType();
    }

    function checkSubtract(registerError, leftType, rightType) {
        registerError("The - operator is incompatible with types %t and %t", [ leftType, rightType ]);
        return typeError.getTypeErrorType();
    }
}