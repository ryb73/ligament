"use strict";

const   babylon          = require("babylon"),
        _                = require("lodash"),
        coreTypes        = require("ligament-core-types"),
        number           = coreTypes.number,
        string           = coreTypes.string,
        ValueType        = coreTypes.ValueType,
        Scope            = require("./scope"),
        FilterAggregator = require("./filter-aggregator");

function Checker($code, $filterNames) {
    let code        = $code,
        filterNames = $filterNames;
    $code = $filterNames = null;

    let types,
        errors = [];

    let self = { check };
    return self;

    function check() {
        let ast = babylon.parse(code, {
            sourceType: "module"
        });

        iterateAst(ast);

        Object.defineProperties(self, {
            types: { get: () => _.cloneDeep(types) },
            errors: { get: () => _.cloneDeep(errors) }
        });
    }

    function iterateAst(ast) {
        let globalScope = {
            __NUMBER: number.getNumberType(),
            __STRING: string.getStringType(),
            undefined: new ValueType(undefined),
        };

        let filterAggregator = new FilterAggregator(filterNames);

        let scope = new Scope(ast.program.body, registerError, globalScope, filterAggregator);
        scope.check();
        types = scope.types;
    }

    function registerError(node, message, types) {
        errors.push({
            message, node,
            types: types || []
        });
    }
}

module.exports = Checker;