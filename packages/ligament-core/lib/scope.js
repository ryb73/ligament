"use strict";

module.exports = Scope;

const   _            = require("lodash"),
        getter       = require("set-getter"),
        coreTypes    = require("ligament-core-types"),
        boolean      = coreTypes.boolean,
        typeError    = coreTypes.typeError,
        getValueType = coreTypes.ValueType,
        OrType       = coreTypes.OrType;

function Scope($body, $registerError, $parentTypes, $filters) {
    let body          = $body,
        registerError = $registerError,
        parentTypes   = _.clone($parentTypes),
        filters       = $filters;
    $body = $registerError = $parentTypes = $filters = null;

    let types = {};

    getter(this, "types", () => types);

    function check() {
        if(body instanceof Array)
            body.forEach(checkNode);
        else
            checkNode(body);

        return parentTypes;
    }
    this.check = check;

    function checkNode(node) {
        switch(node.type) {
            case "VariableDeclaration":
                checkVariableDeclaration(node);
                break;

            case "IfStatement":
                checkIfStatement(node);
                break;

            case "ExpressionStatement":
                checkExpressionStatement(node);
                break;

            case "BlockStatement":
                checkBlockStatement(node);
                break;

            case "EmptyStatement":
                break;

            default:
                registerError(node, `Unhandled node: ${node.type}`);
        }
    }

    function checkVariableDeclaration(node) {
        node.declarations.forEach(checkDeclarations);
    }

    function checkDeclarations(declarator) {
        let identifier = declarator.id.name;

        if(!declarator.init) {
            addType(identifier, getValueType(undefined));
            return;
        }

        let valueType = checkExpression(declarator.init);
        addType(identifier, valueType);
    }

    function checkExpression(expression) {
        switch(expression.type) {
            case "StringLiteral":
            case "NumericLiteral":
            case "BooleanLiteral":
                return getValueType(expression.value);

            case "NullLiteral":
                return getValueType(null);

            case "BinaryExpression":
                return checkBinaryExpression(expression);

            case "Identifier":
                return checkIdentifier(expression);

            case "AssignmentExpression":
                return checkAssignmentExpression(expression);

            default:
                registerError(expression, `Unhandled expression type: ${expression.type}`);
                return typeError.getTypeErrorType();
        }
    }

    function checkIdentifier(expression) {
        let identifier = expression.name;
        if(types[identifier])
            return types[identifier];

        if(parentTypes[identifier])
            return parentTypes[identifier];

        registerError(expression, `Variable "${identifier}" not found`);
        return typeError.getTypeErrorType();
    }

    function checkBinaryExpression(expression) {
        switch(expression.operator) {
            case "+":
                return checkAdditionExpression(expression);

            case "-":
                return checkSubtractionExpression(expression);

            case "===":
                return checkEqualityExpression(expression);

            default:
                registerError(expression, `Unhandled operator: ${expression.operator}`);
                return typeError.getTypeErrorType();
        }
    }

    function checkAdditionExpression(expression) {
        let leftType = checkExpression(expression.left);
        let rightType = checkExpression(expression.right);

        return filters.checkAdd(registerError.bind(null, expression), leftType, rightType);
    }

    function checkSubtractionExpression(expression) {
        let leftType = checkExpression(expression.left);
        let rightType = checkExpression(expression.right);

        return filters.checkSubtract(registerError.bind(null, expression), leftType, rightType);
    }

    function checkEqualityExpression(expression) {
        let leftType = checkExpression(expression.left);
        let rightType = checkExpression(expression.right);

        let resultingType = filters.checkStrictEquality(registerError.bind(null, expression), leftType, rightType);
        if(resultingType)
            return resultingType;

        registerError(expression, "Type %t can never be equal to type %t", [ leftType, rightType ]);
        return boolean.getBooleanType();
    }

    function checkAssignmentExpression(expression) {
        let identifier = expression.left.name;
        let valueType = checkExpression(expression.right);

        if(!setType(identifier, valueType))
            registerError(expression.left, `\`${identifier}\` has not been declared`);
    }

    function checkIfStatement(ifStatement) {
        let testType = checkExpression(ifStatement.test);

        let testTruthiness = filters.checkTruthiness(
            registerError.bind(null, ifStatement.test), testType
        );

        if(testTruthiness.isValue) {
            registerError(ifStatement.test, `This if check will always be ${!!testTruthiness.value}`);
            let branch = testTruthiness.value ? ifStatement.consequent : ifStatement.alternate;
            if(branch)
                checkNode(branch);

            return;
        }

        // Can't determine result of if check, so we'll type both branches and OR the result
        let mergedTypes = mergeWithAncestorTypes();

        let consequentScope = new Scope(ifStatement.consequent, registerError, mergedTypes, filters);
        let consequentTypeResult = consequentScope.check();

        let alternateTypeResult;
        if(ifStatement.alternate) {
            let alternateScope = new Scope(ifStatement.alternate, registerError, mergedTypes, filters);
            alternateTypeResult = alternateScope.check();
        }

        for(let identifier in mergedTypes) {
            let alternateType = (alternateTypeResult) ? alternateTypeResult[identifier] : mergedTypes[identifier];
            let orType = new OrType(consequentTypeResult[identifier], alternateType);
            setType(identifier, orType);
        }
    }

    function checkExpressionStatement(node) {
        checkExpression(node.expression);
    }

    function mergeWithAncestorTypes() {
        let mergedTypes = Object.create(parentTypes);
        return Object.assign(mergedTypes, types);
    }

    function checkBlockStatement(block) {
        let mergedTypes = mergeWithAncestorTypes();

        let blockScope = new Scope(block.body, registerError, mergedTypes, filters);
        let resultingTypes = blockScope.check();

        for(let identifier in mergedTypes) {
            setType(identifier, resultingTypes[identifier]);
        }
    }

    function addType(identifier, type) {
        types[identifier] = type;
    }

    function setType(identifier, type) {
        if(types[identifier]) {
            types[identifier] = type;
            return true;
        }

        if(parentTypes[identifier]) {
            parentTypes[identifier] = type;
            return true;
        }

        return false;
    }
}