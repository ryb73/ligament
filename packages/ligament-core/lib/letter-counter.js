"use strict";

const A_ASCII = 65;

function LetterCounter() {
    if(!this)
        return new LetterCounter();

    let letterIdx = -1,
        cycle = 0;

    return { next };

    function next() {
        ++letterIdx;
        if(letterIdx > 25) {
            letterIdx = 0;
            ++cycle;
        }

        let letter = String.fromCharCode(A_ASCII + letterIdx);
        if(cycle > 0)
            return letter + cycle;

        return letter;
    }
}

module.exports = LetterCounter;