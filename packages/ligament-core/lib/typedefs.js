"use strict";

let any = {
    capabilities: {
        numAdd: true,
        stringAdd: true,
    },
};

module.exports = {
    any,
    checkError: any,

    string: {
        properties: {
            anchor: any,
            charAt:  any,
            charCodeAt: any,
            codePointAt: any,
            concat: any,
            endsWith: any,
            includes: any,
            indexOf: any,
            lastIndexOf: any,
            link: any,
            substr: any,
            substring: any,
            toLocaleLowerCase: any,
            toLocaleUpperCase: any,
            toLowerCase: any,
            toString: any,
            toUpperCase: any,
            trim: any,
            trimLeft: any,
            trimRight: any,
            valueOf: any,
            raw: any,
        },

        capabilities: {
            stringAdd: true,
        },
    },

    number: {
        capabilities: {
            numAdd: true,
        },
    },

    boolean: {
        capabilities: {},
    },
};
