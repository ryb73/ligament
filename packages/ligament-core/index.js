"use strict";

module.exports = {
    Checker: require("./lib/checker"),
    ErrorReporter: require("./lib/error-reporter")
};