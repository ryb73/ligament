"use strict";

const   ava           = require("ava"),
        flatFileTests = require("flat-file-tests"),
        core          = require(".."),
        Checker       = core.Checker;

let tests = flatFileTests.iterate(__dirname + "/fixtures", [ "test.js", "result.json" ]);
for(let test of tests) {
    ava.test(test.getName(), runTest.bind(null, test));
}

async function runTest(testCase, t) {
    let code = await testCase.get("test.js");

    let checker = new Checker(code, [ "compatible-types-filter", "constant-filter", "core-type-filter" ]);
    checker.check();

    let actual = {
        success: checker.errors.length === 0,
        errors: checker.errors.map(formatError.bind(null, code))
    };

    let expected = JSON.parse(await testCase.get("result.json"));

    t.deepEqual(actual, expected);
}

function formatError(code, error) {
    return {
        message: error.message,
        node: code.substring(error.node.start, error.node.end)
    };
}