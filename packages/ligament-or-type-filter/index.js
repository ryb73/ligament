"use strict";

module.exports = OrTypeFilter;

const   coreTypes = require("ligament-core-types"),
        OrType    = coreTypes.OrType;

function OrTypeFilter($filter) {
    let filter = $filter;
    $filter = null;

    return { checkTruthiness, checkAdd, checkSubtract, checkStrictEquality };

    function isOrType(type) {
        return !!type.getPossibleTypes;
    }

    function checkTruthiness(registerError, type) {
        if(!isOrType(type))
            return;

        let resultingTypes = type.getPossibleTypes().map((componentType) => {
            return filter.checkTruthiness(facadeRegisterError, componentType);

            function facadeRegisterError(message, errTypes) {
                return registerError(
                    message,
                    errTypes.map((errType) => {
                        if(errType === componentType)
                            return type;
                        else
                            return errType;
                    })
                );
            }
        });

        return new OrType(...resultingTypes);
    }

    function checkAdd(registerError, leftType, rightType) {
        // registerError("The + operator is incompatible with types %t and %t", [ leftType, rightType ]);
        // return typeError.getTypeErrorType();
    }

    function checkSubtract(registerError, leftType, rightType) {
        // registerError("The - operator is incompatible with types %t and %t", [ leftType, rightType ]);
        // return typeError.getTypeErrorType();
    }

    function checkStrictEquality(registerError, leftType, rightType) {
        // registerError("The - operator is incompatible with types %t and %t", [ leftType, rightType ]);
        // return typeError.getTypeErrorType();
    }
}