"use strict";

module.exports = {
    any:       require("./lib/any"),
    boolean:   require("./lib/boolean"),
    number:    require("./lib/number"),
    OrType:    require("./lib/or-type"),
    string:    require("./lib/string"),
    typeError: require("./lib/type-error"),
    ValueType: require("./lib/value-type")
};