"use strict";

module.exports = ValueType;

const   boolean              = require("./boolean"),
        string               = require("./string"),
        number               = require("./number"),
        defaultTypeFunctions = require("./default-type-functions");

const cache = new Map();

function ValueType(value) {
    if (!this)
        return new ValueType(value);

    if(cache.has(value))
        return cache.get(value);

    this._init(value);

    cache.set(value, this);
}

ValueType.prototype = Object.create(defaultTypeFunctions);

Object.assign(ValueType.prototype,
    {
        _init(value) {
            this.isValue = true;
            this.value = value;

            this.capabilities = {};
            this.properties = {};

            switch(typeof value) {
                case "boolean":
                    boolean.booleanize(this);
                    break;

                case "number":
                    number.numberize(this);
                    break;

                case "string":
                    string.stringize(this);
                    break;

                case "undefined":
                    this.coreType = "undefined";
                    break;

                case "object":
                    this.coreType = "object";
                    break;

                default:
                    throw new Error(`Invalid literal value: ${value}`);
            }
        },

        getPrettyName() {
            if(this.coreType === "undefined")
                return "undefined";

            if(this.value === null)
                return "null";

            return `${this.coreType}(${this.getRawValue()})`;
        },

        checkAdd(registerError, type) {
            switch(this.coreType) {
                case "boolean":
                    return boolean.getBooleanType().checkAdd(registerError, type);

                case "number":
                    return this.checkAddNumber(registerError, type);

                case "string":
                    return this.checkAddString(registerError, type);

                default:
                    return defaultTypeFunctions.checkAdd(registerError, type);
            }
        },

        checkAddNumber(registerError, type) {
            if(!type.isValue || type.coreType !== "number")
                return number.getNumberType().checkAdd(registerError, type);

            return new ValueType(this.value + type.value);
        },

        checkAddString(registerError, type) {
            if(!type.isValue || type.coreType !== "string")
                return string.getStringType().checkAdd(registerError, type);

            return new ValueType(this.value + type.value);
        },

        getRawValue() {
            switch(this.coreType) {
                case "boolean":
                case "number":
                case "undefined":
                case "null":
                    return this.value;

                case "string":
                    return `"${this.value}"`;
            }
        },

        getValue() {
            return this.value;
        },

        getCoreType() {
            return this.coreType;
        }
    }
);