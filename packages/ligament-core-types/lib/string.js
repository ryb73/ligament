"use strict";

module.exports = { getStringType, stringize };

const   boolean              = require("./boolean"),
        typeError            = require("./type-error"),
        defaultTypeFunctions = require("./default-type-functions");

let typeFunctions = {
    checkAdd(registerError, type) {
        if(!type.any && type.coreType !== "string") {
            registerError("Strings can only be added to other strings – type %t is incompatible", [ type ]);
            return typeError.getTypeErrorType();
        }

        return getStringType();
    },

    getPrettyName() {
        return "string";
    },

    getCoreType() {
        return "string";
    }
};

function getStringType() {
    let type = {};

    for(let key in defaultTypeFunctions) {
        if(!typeFunctions[key])
            type[key] = defaultTypeFunctions[key];
    }

    return stringize(type);
}

function stringize(obj) {
    obj.coreType = "string";

    obj.properties = obj.properties || {};
    obj.properties.anchor = { any: true };
    obj.properties.charAt = { any: true };
    obj.properties.charCodeAt = { any: true };
    obj.properties.codePointAt = { any: true };
    obj.properties.concat = { any: true };
    obj.properties.endsWith = { any: true };
    obj.properties.includes = { any: true };
    obj.properties.indexOf = { any: true };
    obj.properties.lastIndexOf = { any: true };
    obj.properties.link = { any: true };
    obj.properties.substr = { any: true };
    obj.properties.substring = { any: true };
    obj.properties.toLocaleLowerCase = { any: true };
    obj.properties.toLocaleUpperCase = { any: true };
    obj.properties.toLowerCase = { any: true };
    obj.properties.toString = { any: true };
    obj.properties.toUpperCase = { any: true };
    obj.properties.trim = { any: true };
    obj.properties.trimLeft = { any: true };
    obj.properties.trimRight = { any: true };
    obj.properties.valueOf = { any: true };
    obj.properties.raw = { any: true };

    obj.capabilities = obj.capabilities || {};
    obj.capabilities.stringAdd = true;

    for(let key in typeFunctions) {
        if(!obj[key])
            obj[key] = typeFunctions[key];
    }

    return obj;
}