"use strict";

module.exports = { getNumberType, numberize };

const   boolean              = require("./boolean"),
        typeError            = require("./type-error"),
        defaultTypeFunctions = require("./default-type-functions");

function getNumberType() {
    let type = {};

    for(let key in defaultTypeFunctions) {
        if(!typeFunctions[key])
            type[key] = defaultTypeFunctions[key];
    }

    return numberize(type);
}

let typeFunctions = {
    checkAdd(registerError, type) {
        if(!type.any && type.coreType !== "number") {
            registerError("Numbers can only be added to other numbers – type %t is incompatible", [ type ]);
            return typeError.getTypeErrorType();
        }

        return getNumberType();
    },

    getPrettyName() {
        return "number";
    },

    getCoreType() {
        return "number";
    }
};

function numberize(obj) {
    obj.coreType = "number";

    obj.capabilities = obj.capabilities || {};
    obj.capabilities.numAdd = true;

    for(let key in typeFunctions) {
        if(!obj[key])
            obj[key] = typeFunctions[key];
    }

    return obj;
}
