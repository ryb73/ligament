"use strict";

module.exports = OrType;

const _ = require("lodash");

function OrType(...$types) {
    let types = _.clone($types);
    $types = null;

    if(_.uniq(types).length === 1)
        return types[0];

    return { getPrettyName, getCoreType, getPossibleTypes };

    function getPrettyName() {
        let prettyNames = _.invokeMap(types, "getPrettyName");
        return prettyNames.join(" | ");
    }

    function getCoreType() {
        let coreTypes = _(types)
            .invokeMap("getCoreType")
            .uniq()
            .value();

        if(coreTypes.length !== 1)
            return null;

        return coreTypes[0];
    }

    function getPossibleTypes() {
        return _.clone(types);
    }
}