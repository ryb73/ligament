"use strict";

module.exports = { getTypeErrorType };

const any = require("./any");

function getTypeErrorType() {
    return any.getAnyType();
}