"use strict";

module.exports = { getBooleanType, booleanize };

const defaultTypeFunctions = require("./default-type-functions");

function getBooleanType() {
    return booleanize(Object.create(defaultTypeFunctions));
}

function booleanize(obj) {
    obj.coreType = "boolean";

    obj.getPrettyName = obj.getPrettyName || getPrettyName;

    obj.capabilities = obj.capabilities || {};

    return obj;
}

function getPrettyName() {
    return "boolean";
}
