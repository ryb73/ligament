"use strict";

module.exports = {};

const typeError = require("./type-error");

Object.assign(module.exports, {
    checkAdd(registerError, type) {
        registerError("The + operator is incompatible with type %t", [ this ]);
        return typeError.getTypeErrorType();
    },

    // getPrettyName() {
    //     return JSON.stringify(this, null, 2);
    // }

    getCoreType() {
        return null;
    }
});
