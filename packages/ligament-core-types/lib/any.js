"use strict";

module.exports = { getAnyType };

const defaultTypeFunctions = require("./default-type-functions");

function getAnyType() {
    let type = Object.create(defaultTypeFunctions);

    Object.assign(type, {
        any: true,

        capabilities: {
            numAdd: true,
            stringAdd: true,
        },

        getPrettyName: () => "any"
    });

    return type;
}
