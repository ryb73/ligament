"use strict";

module.exports = ConstantFilter;

const ValueType = require("ligament-core-types").ValueType;

function ConstantFilter() {
    return { checkTruthiness, checkAdd, checkSubtract, checkStrictEquality };

    function isValueType(type) {
        return !!type.getValue;
    }

    function checkTruthiness(registerError, type) {
        if(!isValueType(type))
            return;

        return new ValueType(!!type.getValue());
    }

    function checkAdd(registerError, leftType, rightType) {
        if(!isValueType(leftType) || !isValueType(rightType))
            return;

        return new ValueType(leftType.getValue() + rightType.getValue());
    }

    function checkSubtract(registerError, leftType, rightType) {
        if(!isValueType(leftType) || !isValueType(rightType))
            return;

        return new ValueType(leftType.getValue() - rightType.getValue());
    }

    function checkStrictEquality(registerError, leftType, rightType) {
        if(!isValueType(leftType) || !isValueType(rightType))
            return;

        return new ValueType(leftType.getValue() === rightType.getValue());
    }
}