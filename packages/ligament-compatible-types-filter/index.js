"use strict";

module.exports = CompatibleTypesFilter;


const   coreTypes = require("ligament-core-types"),
        typeError = coreTypes.typeError,
        boolean   = coreTypes.boolean;

function CompatibleTypesFilter() {
    return { checkAdd, checkSubtract, checkStrictEquality };

    function checkAdd(registerError, leftType, rightType) {
        let leftCoreType = leftType.getCoreType();
        let rightCoreType = rightType.getCoreType();
        if(leftCoreType !== rightCoreType && !leftType.any && !rightType.any) {
            if(leftCoreType === "string")
                return doStringError(registerError, rightType);

            if(leftCoreType === "number")
                return doNumberError(registerError, rightType, "added to");

            if(rightCoreType === "string")
                return doStringError(registerError, leftType);

            if(rightCoreType === "number")
                return doNumberError(registerError, leftType, "added to");
        }
    }

    function checkSubtract(registerError, leftType, rightType) {
        let leftCoreType = leftType.getCoreType();
        let rightCoreType = rightType.getCoreType();
        if(leftCoreType !== rightCoreType && !leftType.any && !rightType.any) {
            if(leftCoreType === "number")
                return doNumberError(registerError, rightType, "subtracted from");

            if(rightCoreType === "number")
                return doNumberError(registerError, leftType, "subtracted from");
        }
    }

    function doStringError(registerError, badType) {
        registerError(
            "Strings can only be added to other strings – type %t is incompatible",
            [ badType ]
        );

        return typeError.getTypeErrorType();
    }

    function doNumberError(registerError, badType, operation) {
        registerError(
            "Numbers can only be " + operation + " other numbers – type %t is incompatible",
            [ badType ]
        );

        return typeError.getTypeErrorType();
    }

    function checkStrictEquality(registerError, leftType, rightType) {
        let leftCoreType = leftType.getCoreType();
        let rightCoreType = rightType.getCoreType();
        if(leftCoreType !== rightCoreType && !leftType.any && !rightType.any) {
            if(leftCoreType === "number" || rightCoreType === "number" ||
                leftCoreType === "string" || rightCoreType === "string")
            {
                registerError("Equality error");
                return boolean.getBooleanType();
            }
        }
    }
}